#!/bin/bash

cd /etc/opendkim/keys || exit
opendkim-genkey -b 2048 -h rsa-sha256 -r -s "$3" -d "$2" -v
mv "$3.private" "$1.private"
mv "$3.txt" "$1.txt"
